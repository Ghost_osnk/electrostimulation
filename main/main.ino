#define s_freq 482
#define s_voltage 5

bool dict = true;
bool dict_1 = true;

void setup() 
{ 
  Serial.begin(9600);
  Serial1.begin(9600);
  DDRF = 0xFF;
}

#define offset 128

void loop() { 
  sig_sin(4, 5, 0);
}

void sig_sin(int freq, float max_amp, float shift_amp) {
  float period = s_freq / (50*6);
  float step_x = 2 * PI / 50;
//  Serial1.print("step_x: ");
//  Serial1.println(step_x);  
  float amplitude = max_amp * 255 / s_voltage;
  float shift_amplitude = shift_amp * 255 / s_voltage;
  float cur_lvl_x = 0;
  float cur_lvl_x_1 = 0 + step_x * 20;
  while (true){
    if (dict){
      Serial1.write(int(amplitude * sin(cur_lvl_x) + shift_amplitude));
    }
    else{
      Serial1.write(int((amplitude * sin(cur_lvl_x)) + shift_amplitude));
    }

    if (dict_1){
      Serial.println(int(amplitude * sin(cur_lvl_x_1) + shift_amplitude));
    }
    else{
      Serial.println(int(amplitude * sin(cur_lvl_x_1) + shift_amplitude));
    }
    cur_lvl_x += step_x;
    cur_lvl_x_1 += step_x;
    
    if (cur_lvl_x >= PI + step_x)
    {
      dict = !dict;
      cur_lvl_x = step_x;
    }
    
    if (cur_lvl_x_1 >= PI + step_x)
    {
      dict_1 = !dict_1;
      cur_lvl_x_1 = step_x;
    }
  }
}


void sig_meandr(int freq, float max_amp, float shift_amp) {
  int period = int(s_freq / freq / 2);
  float amplitude = max_amp * 255 / s_voltage;
  float shift_amplitude = shift_amp * 255 / s_voltage;
  Serial.print(period);
  Serial.print(',');
  Serial.print(amplitude);
  Serial.print(',');
  Serial.println(shift_amp);
  bool flag = true;
  while (digitalRead(5)) {
    for (int i = 0; i < period; i++) {
          
      if (flag){
                Serial.println(amplitude + shift_amplitude);
        PORTA = (char(int(amplitude + shift_amplitude)));
      }

      else{        Serial.println(0 + shift_amplitude);
        PORTA = (char(int(0 + shift_amplitude)));}

        delayMicroseconds(75);
}
    if (flag)
      flag = false;
    else
      flag = true;
  }
}

void sig_row(int freq, float max_amp, float shift_amp) {
  float period = s_freq / freq;
  float amplitude = max_amp * 255 / s_voltage;
  float step_sig = amplitude / period;
  float cur_lvl = amplitude;
  float shift_amplitude = shift_amp * 255 / s_voltage;
  bool flag = true;
  while (true){
    Serial.println(cur_lvl + shift_amplitude);
    PORTA = (char(int(cur_lvl + shift_amplitude)));
    delayMicroseconds(75);
    cur_lvl -= step_sig;
    if (cur_lvl < 0)
      cur_lvl = amplitude;
  }
}

void sig_triangle(int freq, float max_amp, float shift_amp) {  
  float period = s_freq / freq / 2;
  float amplitude = max_amp * 255 / s_voltage;
  float step_y = amplitude * 2 / period;
  float shift_amplitude = shift_amp * 255 / s_voltage;
  float cur_lvl = 0;
  bool flag = true;
  while (true){
    if (flag){
      flag = false;
      while (cur_lvl < amplitude - step_y)
      {
        cur_lvl += step_y;
        Serial.println(cur_lvl + shift_amplitude);
        PORTA = (char(int(cur_lvl + shift_amplitude)));
      }
    }
    else{
      flag = true;
      while (cur_lvl > step_y)
      {
        cur_lvl -= step_y;
        Serial.println(cur_lvl + shift_amplitude);
        PORTA = (char(int(cur_lvl + shift_amplitude)));
    
      }
    }
    delayMicroseconds(75);
  }
}

void sig_trap(int freq, float max_amp, float shift_amp) {
  float period = s_freq / freq / 3;
  float amplitude = max_amp * 255 / s_voltage;
  float step_y = amplitude * 3 / period;
  float shift_amplitude = shift_amp * 255 / s_voltage;
  float cur_lvl = 0;
  bool flag = true;
  while (true){
    if (flag){
      flag = false;
      while (cur_lvl < amplitude)
      {
        cur_lvl += step_y;
        Serial.println(cur_lvl + shift_amplitude);
      }
      float cur_lvl = 0;
      while (cur_lvl <= amplitude - step_y)
      {
        cur_lvl += step_y;
        Serial.println(amplitude + shift_amplitude);
        PORTA = (char(int(cur_lvl + shift_amplitude)));
      }
    }
    else{
      flag = true;
      while (cur_lvl >= step_y)
      {
        cur_lvl -= step_y;
        Serial.println(cur_lvl + shift_amplitude);
        PORTA = (char(int(cur_lvl + shift_amplitude)));
      }
    }
    delayMicroseconds(75);
  }  
}

void sig_exp(int freq, float max_amp, float shift_amp) {
  float period = s_freq / freq;
  float amplitude = max_amp * 255 / s_voltage;
  float shift_amplitude = shift_amp * 255 / s_voltage;
  float cur_lvl_x = 0;
  while (true){
    cur_lvl_x ++;
    if(cur_lvl_x >= period)
    {
      PORTA = (char(int(amplitude + shift_amplitude)));
      delayMicroseconds(75);
      PORTA = (char(int(amplitude + shift_amplitude)));
      delayMicroseconds(75);
      cur_lvl_x = 1;
    }      
    PORTA = (char(int(amplitude * exp(-cur_lvl_x / 5) + shift_amplitude)));
  }
  delayMicroseconds(75);
  
//  x++;
//  if (x > 21)
//  {
//    Serial.println(0);
//    Serial.println(1);
//    x = 1;
//  }
//  Serial.println(exp(-x / 5));
}
