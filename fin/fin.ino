#include <GyverTimers.h>

#define iFreq 428
#define iVolt 255
#define dVolt 255
#define dShift 0

#define encInterPin = 0
#define tumbInterPin = 1
#define butInterPin = 2

#define sbPin_0 = 0
#define sbPin_1 = 1
#define sbPin_2 = 2
#define sbPin_3 = 3

#define encClkPin = 0
#define encDtPin = 0

#define ldPin_0 = 0
#define ldPin_1 = 1
#define speaker = 2

#define blthKey = 0

//iFreq: initial (crystall) Frequency 
//iVolt: initial maximum Voltage
//dVolt: default Voltage
//dShift: default Shift
//--------------------------------------------------------------------------------
//encInterPin: Pin of Interrupt for encoder
//tumbInterPin: Pin of Interrupt for tumbler
//butInterPin: Pin of Interrupt for button
//--------------------------------------------------------------------------------
//sbPin_0: Pin of signal for button_0 (SB_0)
//sbPin_1: Pin of signal for button_1 (SB_1)
//sbPin_2: Pin of signal for button_2 (SB_2)
//sbPin_3: Pin of signal for button_3 (SB_3)
//--------------------------------------------------------------------------------
//encClkPin: Pin of signal for CLK-pin of encoder
//encDtPin: Pin of signal for DT-pin of encoder
//--------------------------------------------------------------------------------
//ldPin_0: Pin of signal for luminodiode_0 (HL_0)
//ldPin_1: Pin of signal for luminodiode_1 (HL_1)
//speaker: Pin of signal for speaker
//--------------------------------------------------------------------------------
//blthKey: Pin of signal for KEY-pin of bluetooth-module

unsigned int nTime, nMinTime, nSecTime, cTime, nModeS;
bool fGenSig = false;
float nFreq, nVoltA, nVoltP, nVoltN, nShiftVA, nShiftVP, nShiftVN, nShiftS;

//nTime: need Time
//nMinTime: need Minutes Time
//nSecTime: need Seconds Time
//cTime: current Time
//nModeS: need Mode Signal
//--------------------------------------------------------------------------------
//fGenSig: flag Generation Signal
//--------------------------------------------------------------------------------
//nFreq: need Frequency
//nVoltA: need Voltage All
//nVoltP: need Voltage Positive
//nVoltN: need Voltage Negative
//nShiftVA: need Shift Voltage All
//nShiftVP: need Shift Voltage Positive
//nShiftVN: need Shift Voltage Negative
//nShiftS: need Shift Signal
//--------------------------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);

  //Serial: debug
  //Serial1: blth
  //Serial2: display

  DDRE = 0xFF;
  DDRA = 0xFF;
  DDRK = 0xFF;
  DDRF = 0xFF;
  
  //DDRE: to 1 DAC
  //DDRA: to 2 DAC
  //DDRK: to 3 DAC
  //DDRF: to 4 DAC
  
  Timer3.setFrequency(1);
  Timer3.enableISR(CHANNEL_A);
  Timer3.stop();

  attachInterrupt(0, encPush, FALLING);
  attachInterrupt(1, tumbPush, CHANGE);
  attachInterrupt(2, butPush, FALLING);
}

ISR(TIMER3_A){
  cTime++;
  if (cTime == nTime)
  {
    fGenSig = false;
    Timer3.stop();
  }    
}

void encPush(){
  
}

void tumbPush(){
  fGenSig = !fGenSig;
  if (fGenSig){
    if (nVoltP == dVolt && nVoltN == dVolt)
      nVoltP = nVoltN = nVoltA;
    if (nShiftVA > dShift && nShiftVP == dShift)
      nShiftVP = nShiftVA;
    else if (nShiftVA < dShift && nShiftVN == dShift)
        nShiftVN = nShiftVA;
      else if ((nShiftVP != dShift || nShiftVN != dShift) && nShiftVA != dShift)
          nShiftVP = nShiftVN = nShiftVA;
        
    switch (nModeS){
      case 0:
        sinusSig();
        break;
      
      case 1:
        meandrSig();
        break;
        
      case 2:    
        rowSig();
        break;
        
      case 3:
        expSig();
        break;
        
      case 4:
        triangleSig();
        break;
        
      case 5:
        trapezoidSig();
        break;
    }
  }
}

void butPush(){
  
}

void bluetooth(){
  
}

void screen(){
  
}

void sinusSig(){
  float stepX = 2 * PI / (iFreq / nFreq);
  float cVoltP = nVoltP * iVolt;
  float cVoltN = nVoltN * iVolt;
  float cShiftVP = nShiftVP;
  float cShiftVN = nShiftVN;
  float curX_0 = 0;
  float curX_1 = nShiftS * stepX;
  bool dict_0 = true;
  bool dict_1 = true;
  while (fGenSig){
//    if (dict_0)
//      PORTE = char(int(cVoltP * sin(curX_0) + cShiftVP));
//    else
//      PORTA = char(int(cVoltN * sin(curX_0) + cShiftVN));
//    if (dict_1)
//      PORTK = char(int(cVoltP * sin(curX_1) + cShiftVP));
//    else
//      PORTF = char(int(cVoltN * sin(curX_1) + cShiftVN));

    if (dict_0)
      Serial.write(int(cVoltP * sin(curX_0) + cShiftVP));
    else
      Serial.write(int(cVoltN * sin(curX_0) + cShiftVN));
    if (dict_1)
      Serial.write(int(cVoltP * sin(curX_1) + cShiftVP));
    else
      Serial.write(int(cVoltN * sin(curX_1) + cShiftVN));
    curX_0 += stepX;
    curX_1 += stepX;

    if (curX_0 >= PI + stepX)
    {
      dict_0 = !dict_0;
      curX_0 = stepX;
    }
    
    if (curX_1 >= PI + stepX)
    {
      dict_1 = !dict_1;
      curX_1 = stepX;
    }
  }
}

void meandrSig(){
  float half_period = iFreq / nFreq / 2;
  float cVoltP = nVoltP * iVolt;
  float cVoltN = nVoltN * iVolt;
  float cShiftVP = nShiftVP;
  float cShiftVN = nShiftVN;
  float curX_0 = 0;
  float curX_1 = nShiftS;
  bool dict_0 = true;
  bool dict_1 = true;

  while (fGenSig){
//    if (dict_0)
//      PORTE = char(int(cVoltP * sin(curX_0) + cShiftVP));
//    else
//      PORTA = char(int(cVoltN * sin(curX_0) + cShiftVN));
//    if (dict_1)
//      PORTK = char(int(cVoltP * sin(curX_1) + cShiftVP));
//    else
//      PORTF = char(int(cVoltN * sin(curX_1) + cShiftVN));

    if (dict_0)
      Serial.write(int(cVoltP + cShiftVP));
    else
      Serial.write(int(cVoltN + cShiftVN));
    if (dict_1)
      Serial.write(int(cVoltP + cShiftVP));
    else
      Serial.write(int(cVoltN + cShiftVN));
    curX_0 ++;
    curX_1 ++;

    if (curX_0 >= half_period)
    {
      dict_0 = !dict_0;
      curX_0 = 1;
    }
    
    if (curX_1 >= half_period)
    {
      dict_1 = !dict_1;
      curX_1 = 1;
    }
  }
}

void rowSig(){  
  float period = iFreq / nFreq;
  float half_period = period / 2;
  period--;
  float cVoltP = nVoltP * iVolt;
  float cVoltN = nVoltN * iVolt;
  float cShiftVP = nShiftVP;
  float cShiftVN = nShiftVN;
  float stepX = (cVoltP + cShiftVP + cVoltN + cShiftVN) / period;
  float curX_0 = cVoltN + cShiftVN;
  float curX_1 = cVoltN + cShiftVN - stepX * nShiftS;
  bool dict_0 = true;
  bool dict_1 = true;

  while (fGenSig){
//    if (dict_0)
//      PORTE = char(int(cVoltP * sin(curX_0) + cShiftVP));
//    else
//      PORTA = char(int(cVoltN * sin(curX_0) + cShiftVN));
//    if (dict_1)
//      PORTK = char(int(cVoltP * sin(curX_1) + cShiftVP));
//    else
//      PORTF = char(int(cVoltN * sin(curX_1) + cShiftVN));

    if (dict_0)
    {      
      Serial.write(int(curX_0));
      curX_0 -= stepX;
    }
    else{      
      Serial.write(int(curX_0));
      curX_0 += stepX;
    }
    if (dict_1){      
      Serial.write(int(curX_1));
      curX_1 -= stepX;
    }
    else{      
      Serial.write(int(curX_1));
      curX_1 += stepX;
    }

    if (curX_0 == half_period)
      dict_0 = true;
    if (curX_1 == half_period)
      dict_1 = true;

    if (curX_0 == period)
    {
      dict_0 = false;
      curX_0 = cVoltN + cShiftVN;
    }
    
    if (curX_1 == period)
    {
      dict_1 = false;
      curX_1 = cVoltN + cShiftVN;
    }
  }
}

void triangleSig(){
  float period = iFreq / nFreq;
  float half_period = period / 2;  
  float quarter_period = period / 4;
  float cVoltP = nVoltP * iVolt;
  float cVoltN = nVoltN * iVolt;
  float cShiftVP = nShiftVP;
  float cShiftVN = nShiftVN;
  float stepX = (cVoltP + cShiftVP + cVoltN + cShiftVN) / half_period;
  float curX_0 = cVoltN + cShiftVN;
  float curX_1 = cVoltN + cShiftVN - stepX * nShiftS;
  bool dict_0 = true;
  bool dict_1 = true;
  
  while (fGenSig){
//    if (dict_0)
//      PORTE = char(int(cVoltP * sin(curX_0) + cShiftVP));
//    else
//      PORTA = char(int(cVoltN * sin(curX_0) + cShiftVN));
//    if (dict_1)
//      PORTK = char(int(cVoltP * sin(curX_1) + cShiftVP));
//    else
//      PORTF = char(int(cVoltN * sin(curX_1) + cShiftVN));

    if (dict_0)
      Serial.write(int(curX_0));
    else
      Serial.write(int(curX_0));
    if (dict_1)     
      Serial.write(int(curX_1));
    else
      Serial.write(int(curX_1));


    if (curX_0 <= quarter_period){
      if (dict_0)
        dict_0 = false;
      curX_0 -= stepX;
    }
    else if (curX_0 >= quarter_period && curX_0 <= half_period){
      if (!dict_0)
        dict_0 = true;
      curX_0 += stepX; 
    }
    else if (curX_0 >= half_period && curX_0 <= period){
      if (!dict_0)
        dict_0 = true;
      curX_0 -= stepX; 
    }
    else if (curX_0 < period){
      if (dict_0)
        dict_0 = false;
      curX_0 += stepX;
    }
    else if (curX_0 == period){
      if (dict_0)
        dict_0 = false;
      curX_0 = cVoltN + cShiftVN;
    }

    if (curX_1 <= quarter_period){
      if (dict_1)
        dict_1 = false;
      curX_1 -= stepX;
    }
    else if (curX_1 >= quarter_period && curX_1 <= half_period){
      if (!dict_1)
        dict_1 = true;
      curX_1 += stepX; 
    }
    else if (curX_1 >= half_period && curX_1 <= period){
      if (!dict_1)
        dict_1 = true;
      curX_1 -= stepX; 
    }
    else if (curX_1 < period){
      if (dict_1)
        dict_1 = false;
      curX_1 += stepX;
    }
    else if (curX_1 == period)
    {
      if (dict_1)
        dict_1 = false;
      curX_1 = cVoltN + cShiftVN - stepX * nShiftS;
    }
    
  }
}

void trapezoidSig(){
  
}

void expSig(){
  
}

void loop() {

}
