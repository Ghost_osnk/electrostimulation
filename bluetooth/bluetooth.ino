#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // указываем пины rx и tx соответственно

void setup()  {
  pinMode(10,INPUT);
  pinMode(11,OUTPUT);
  Serial.begin(9600);
  mySerial.begin(9600);
  Serial.println("start prg");
}

void loop() {
  if (mySerial.available()) {
    char c = mySerial.read(); // читаем из software-порта
    Serial.print(c); // пишем в hardware-порт
  }
  if (Serial.available()) {
    char c = Serial.read(); // читаем из hardware-порта
    mySerial.write(c); // пишем в software-порт
  }
}
