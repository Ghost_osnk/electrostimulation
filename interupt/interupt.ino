volatile int counter = 0;
void setup() {
  Serial.begin(9600);
  pinMode(2, INPUT); 
  attachInterrupt(0, buttonTick, FALLING);
}
void buttonTick() {
  counter++;
}
void loop() {
  Serial.println(counter);
  delay(1000);
}
